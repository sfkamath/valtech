package valtech;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CheckoutItemsTest {
	@Test
	public void canAcceptArrayStringsAtCheckout() {
		String[] items = {};
		CheckoutItems ci = new CheckoutItems();
		double cost = ci.getCost(items);
		assertEquals(0.0, cost, 0);
	}
	
	@Test
	public void canAcceptApplesAndOranges() {
		String[] items = {"Apple", "Orange"};
		CheckoutItems ci = new CheckoutItems();
		double cost = ci.getCost(items);
		assertEquals(0.85, cost, 0);
	}
	
	@Test
	public void canIgnoreAnyOtherItem() {
		String[] items = {"Apple", "Orange", "Carrots"};
		CheckoutItems ci = new CheckoutItems();
		double cost = ci.getCost(items);
		assertEquals(0.85, cost, 0);
	}
}
