package valtech;

public class CheckoutItems {
	protected double getCost(String[] items) {
		double cost = 0.0;
		for (String item : items) {
			try {
				Item itemType = Item.valueOf(item.toUpperCase());
				cost += itemType.getCost();
			} catch (java.lang.IllegalArgumentException e) {
				//TODO : do something with exception
			}
		}
		return cost;
	}
	
	public String printPrice(String[] items) {
		return "£" + getCost(items);
	}
	
	public static void main(String[] args) {
		String[] items = {"Apple", "Apple", "Orange", "Apple"};
		CheckoutItems ci = new CheckoutItems();
		System.out.println(ci.printPrice(items));
	}
}
