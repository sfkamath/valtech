package valtech;

public enum Item {
	APPLE(0.60), ORANGE(0.25);
	
	private double cost;
	
	Item(double c) {
		cost = c;
	}
	
	public double getCost() {
		return cost;
	}
}
